1. kubectl create namespace monitoring
2. helm repo add influxdata https://helm.influxdata.com/
3. helm upgrade --namespace monitoring --install influxdb influxdata/influxdb --set resources.requests.memory=1Gi --set resources.requests.cpu=1000m --set persistence.storageClass="local-storage"
4. helm upgrade --namespace monitoring --install grafana grafana/grafana --set persistence.storageClass="local-storage" --set service.type=LoadBalancer --set service.port=3000
5. grafana pass = kubectl get secret --namespace monitoring grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo

tutorial : https://habr.com/ru/post/569124/
